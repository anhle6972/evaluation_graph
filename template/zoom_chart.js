function collectDataXFrames(frame_idx, num_frames, full_data_table) {
    zoom_data_arr = [];
    const header = appendStr('detect_', labels).concat(appendStr('groundtruth_', labels));
    header.unshift('frame_number');
    zoom_data_arr.push(header);

    rowIndex = frame_idx;
    numColumns = MAIN_CHART_DATATABLE_ZOOM_COLOR_COL_IDX;
    for (var r=rowIndex; r<rowIndex+num_frames; r++) {
        var row = []
        for (var c=0; c<numColumns; c++) {
            cell_data = full_data_table.getValue(r, c);
            row.push(cell_data);
        }
        zoom_data_arr.push(row);
    }
    return zoom_data_arr;
}

function refreshZoomChart(data_arr, zoom_chart_type, row_idx_selection, is_one_line) {
    const dataView = new google.visualization.DataView(google.visualization.arrayToDataTable(data_arr, false));
    idx = labels.indexOf(cur_vehicle) + 1;
    dataView.setColumns([0, idx, idx+labels.length]);
    if (is_one_line == true) {
        dataView.hideColumns([idx+labels.length]);          // hide groundtruth line
    }
    
    var zoom_chart = null;
    if (zoom_chart_type == 'line_chart') {
        zoom_chart = new google.visualization.LineChart(document.getElementById('zoom_div'));
    } else if (zoom_chart_type == 'column_chart') {
        zoom_chart = new google.visualization.ColumnChart(document.getElementById('zoom_div'));
    } else if (zoom_chart_type == 'area_chart') {
        zoom_chart = new google.visualization.AreaChart(document.getElementById('zoom_div'));
    } else if (zoom_chart_type == 'candle_stick_chart') {
        zoom_chart = new google.visualization.CandlestickChart(document.getElementById('zoom_div'));
    } else if (zoom_chart_type == 'combo_chart') {
        zoom_chart = new google.visualization.ComboChart(document.getElementById('zoom_div'));
    } else if (zoom_chart_type == 'stepped_area_chart') {
        zoom_chart = new google.visualization.SteppedAreaChart(document.getElementById('zoom_div'));
    }

    const options_zoomplot = {
        title: capitalizeFirstLetter(cur_vehicle) + ' count detection over time (Zoom in)',
        animation: {
            duration: 0,
            easing: 'linear'
        },
        selectionMode: 'single',
        hAxis: {
            title: 'Time of day',
            gridlines: {
                count: 5,
                /*units: {
                    days: {format: ['MMM dd']},
                    hours: {format: ['HH:mm', 'ha']},
                }*/
            },
            /*minorGridlines: {
                units: {
                    hours: {format: ['hh:mm:ss a', 'ha']},
                    minutes: {format: ['HH:mm a Z', ':mm']}
                }
            }*/
        },
        vAxis: {
            viewWindow: {
                min: 0,
                max: max_dict[cur_vehicle]
            },
            title: 'Number of ' + cur_vehicle
        },
        crosshair: {
            trigger: 'selection',
            orientation: 'vertical',
            color: '#000000'
        },
        tooltip: {
            trigger: 'focus'
        },
        colors: [DETECTION_LINE_COLOR]
    };

    zoom_chart.draw(dataView, options_zoomplot);
    zoom_chart.setSelection([{row: row_idx_selection, column: 1}]);    
}
