function appendStr(string, arr) {
    return arr.map(function (e) {
        return string + e;
    })
}

function drawRect(canvas, arr2d, color) {
    ctx = canvas.getContext('2d');
    ctx.beginPath();
    ctx.strokeStyle = color;
    for (let i = 0; i < arr2d.length; i++) {
        const c = arr2d[i];
        ctx.rect(c[0] * canvas.width, c[1] * canvas.height, (c[2] - c[0]) * canvas.width, (c[3] - c[1]) * canvas.height);
        ctx.stroke();
    }
}

function clearCanvas(canvas) {
    context = canvas.getContext('2d');
    context.clearRect(0, 0, canvas.width, canvas.height);
}

function drawImage(canvas, imgIdx) {
    const context = canvas.getContext('2d');
    const background = imgs[imgIdx];
    context.drawImage(background,
        0, 0, background.width, background.height,
        0, 0, canvas.width, canvas.height
    );
}

function img_file(idx) {
    idx = 12 * idx + 1;
    let name = idx + "";
    while (name.length < 6) name = '0' + name;
    //console.log(name);
    return "img_" + name + ".jpg"
}

const img_names = [];
for (let i = 1; i < 5020; i += 12) {
    let name = i + "";
    while (name.length < 6) name = '0' + name;
    img_names.push('img/img_' + name + '.jpg');
}
const imgs = [];
img_names.forEach(function (url) {
    const i = new Image();
    i.src = url;
    imgs.push(i);
});
