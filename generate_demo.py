#!/usr/bin/python3
import sys
import os
import shutil
import os.path as ospath

import pandas as pd
import re
import csv
import numpy as np
from collections import Counter

from collections import OrderedDict

labels = ['pedestrian', 'motorbike', 'car', 'bus', 'truck']
df_detect = pd.DataFrame(columns=['vehicle', 'x', 'y', 't', 'b'])
df_groundtruth = pd.DataFrame(columns=['groundtruth_' + label for label in labels])

from collections import defaultdict
import json

def generate_dict_detect(detection_txt, img_folder):
    dict_detect = {}
    with open(detection_txt, 'r') as f:
        file = csv.reader(f)
        for row in file:
            frame_number = row[0].split('/')[-1]

            #print('os.path.join(img_folder, frame_number)', os.path.join(img_folder, frame_number))
            if not os.path.isfile(os.path.join(img_folder, frame_number)):
                continue

            dict_detect[frame_number] = {label: [] for label in labels}
            boxes = np.array(row[2:]).reshape(-1, 5)  # each row in boxes is an box (type and 4 coordinates)
            for box in boxes:
                vehicle_type = box[0].split('(')[0]
                coordinates = [float(x) for x in box[1:]]
                dict_detect[frame_number][vehicle_type].append(coordinates)
    
    ordered_dict_detect = OrderedDict()
    for k in sorted(dict_detect.keys()):
        ordered_dict_detect[k] = dict_detect[k]
    return ordered_dict_detect

DEMO_FOLDER_PREFIX = 'demo_'
LIB_DIR = 'lib'
MYCONST_TEMPLATE = 'template/myconst.template'
MINIMAL_SUPPORTED_FILES = [
    'mystyle.css',
    'template/chart.js',
    'template/canvas.js',
    'template/index.html',
    'template/zoom_chart.js'
]

# https://gist.github.com/dreikanter/5650973
def copydir(source, dest):
    """Copy a directory structure overwriting existing files"""
    for root, dirs, files in os.walk(source):
        if not os.path.isdir(root):
            os.makedirs(root)

        for file in files:
            rel_path = root.replace(source, '').lstrip(os.sep)
            dest_path = os.path.join(dest, rel_path)

            if not os.path.isdir(dest_path):
                os.makedirs(dest_path)

            shutil.copyfile(os.path.join(root, file), os.path.join(dest_path, file))

def count_files(DIR):
    return len([name for name in os.listdir(DIR) if os.path.isfile(os.path.join(DIR, name))])

def generate(img_folder, detect_result, hour=0, minute=0, second=0, fps=24, img_step=1, name='Detection', crowd_threshold=-1):
    assert img_folder, '$ python generate_demo.py /path/to/img/folder /path/to/detect/result.txt'
    assert detect_result, '$ python generate_demo.py /path/to/img/folder /path/to/detect/result.txt'
    
    if not ospath.isdir(img_folder):
        raise Exception('No image folder found @ %s' % img_folder)

    if not ospath.isfile(detect_result):
        raise Exception('No detect result found @ %s' % detect_result)

    # create result folder
    result_folder = DEMO_FOLDER_PREFIX + ospath.basename(img_folder)
    try:
        shutil.rmtree(result_folder)
    except:
        pass
    os.makedirs(result_folder, exist_ok=True)

    # copy img folder
    copydir(source=img_folder, dest=ospath.join(result_folder, 'img'))

    # copy lib folder
    copydir(source=LIB_DIR, dest=ospath.join(result_folder, ospath.basename(LIB_DIR)))

    # copy files
    for js_file in MINIMAL_SUPPORTED_FILES:
        shutil.copy(src=js_file, dst=result_folder)

    first_file = os.listdir(img_folder)[0]
    with open(MYCONST_TEMPLATE, 'r') as f:
        myconst_template = f.read()
        myconst_js = myconst_template % {
            'hour': hour,
            'minute': minute,
            'second': second,
            'fps': fps,
            'img_prefix': first_file[:-10],
            'img_step': img_step,
            'img_total': count_files(DIR=img_folder),
            'chart_title': name,
            'crowd_threshold': int(crowd_threshold),  # 180517
        }
    with open(ospath.join(result_folder, 'myconst.js'), 'w') as f:
        f.write(myconst_js)

    #shutil.copy(src=detect_result, dst=result_folder)   # copy detect result file (compiled js)
    dict_detect = generate_dict_detect(detection_txt=detect_result, img_folder=img_folder)
    with open(ospath.join(result_folder, 'detect.js'), 'w') as f:
        f.write('var detect_json = ' + json.dumps(dict_detect))


if __name__ == '__main__':
    assert len(sys.argv) > 1, '$ python generate_demo.py /path/to/img/folder /path/to/detect/result.txt'
    img_folder = sys.argv[1] if len(sys.argv) >= 2 else None
    detect_result = sys.argv[2] if len(sys.argv) >= 3 else None
    
    more_info = {
        'hour': 0,
        'minute': 0,
        'second': 0,
        'fps': 24,
        'img_step': 1
        #'name': 'Detection'
    }

    video_name = ''
    if len(sys.argv) >= 4:
        for k in range(3, len(sys.argv)):
            if sys.argv[k].count('=') > 0:
                info_key, info_value = sys.argv[k].split('=')
                if info_key == 'name':
                    video_name = info_value
                else:
                    more_info[info_key] = float(info_value)

    generate(img_folder, detect_result, name=video_name, **more_info)
