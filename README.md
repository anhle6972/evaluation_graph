Run python timeseries.py to create detect.js and groundtruth.js

Open index.html with Firefox browser

Zoom out and refresh for better view

Command to generate the demo app (without groundtruth comparison, contains only prediction line):
```
$ python generate_demo.py /path/to/img/folder /path/to/detect.js
$ python generate_demo.py img ntpn_fps_2/detect_result_ntpn_test_fps_2_roi_filtered.txt name='Nga Tu Phu Nhuan' hour=7 minute=1 second=30 fps=2 img_step=12

$ python generate_demo.py source/N02105558__EVAL_10min/images/N02105558__EVAL_10min_.mp4_1k source/N02105558__EVAL_10min/detection.txt name='Q10-P4 Medic1' hour=10 minute=55 second=59 fps=8 img_step=1
$ python generate_demo.py source/3_05_H_180412220000__TEST_10min/images/3_05_H_180412220000__TEST_10min_.avi_1k source/3_05_H_180412220000__TEST_10min/detection.txt name='Q10-P6 Cam01' hour=21 minute=59 second=59 fps=8 img_step=1
$ python generate_demo.py source/4_10_H_180412150000__TEST_10min/images/4_10_H_180412150000__TEST_10min_.avi_1k source/4_10_H_180412150000__TEST_10min/detection.txt name='Q10-P6 NGUYENLAM' hour=14 minute=59 second=59 fps=8 img_step=1

$ python generate_demo.py source/5_01_H_180412050000__TEST/images/5_01_H_180412050000__TEST_.avi_1k source/5_01_H_180412050000__TEST/detection.txt name='Q10-P6 Quang Trung' hour=4 minute=59 second=59 fps=8 img_step=1
$ python generate_demo.py source/5_20_H_180411120000__TEST/images/5_20_H_180411120000__TEST_.avi_1k source/5_20_H_180411120000__TEST/detection.txt name='Q10-P5 Cam01' hour=11 minute=59 second=59 fps=8 img_step=1
$ python generate_demo.py source/1_01_H_180412170000__TEST/images/1_01_H_180412170000__TEST_.avi_1k source/1_01_H_180412170000__TEST/detection.txt name='Q10-P6 Quang Trung' hour=16 minute=59 second=59 fps=8 img_step=1

$ python generate_demo.py source/nkkn-ntmk-demo-night-15min/images/nkkn-ntmk-demo-night-15min_.mkv source/nkkn-ntmk-demo-night-15min/detection.txt name='NKKN-NTMK 16-04-2018 (Night)' hour=19 minute=59 second=58 fps=2 img_step=1 crowd_threshold=20
$ python generate_demo.py source/nkkn-ntmk-demo-day-15min/images/nkkn-ntmk-demo-day-15min_.mkv source/nkkn-ntmk-demo-day-15min/detection.txt name='NKKN-NTMK 16-04-2018 (Day)' hour=7 minute=59 second=57 fps=2 img_step=1 crowd_threshold=50
$ python generate_demo.py source/nvt-ntt-demo-day-15min/images/nvt-ntt-demo-day-15min_.mkv source/nvt-ntt-demo-day-15min/detection.txt name='NVTroi-NTTuyen 04-04-2018 (Day)' hour=8 minute=9 second=59 fps=2 img_step=1 crowd_threshold=50
$ python generate_demo.py source/nvt-ntt-demo-night-15min/images/nvt-ntt-demo-night-15min_.mkv source/nvt-ntt-demo-night-15min/detection.txt name='NVTroi-NTTuyen 04-04-2018 (Night)' hour=20 minute=10 second=0 fps=2 img_step=1 crowd_threshold=25
```

Command to copy the first 500 images:
```
$ ls images/N02105558__EVAL_10min_.mp4_1k/ | head -n 500 | xargs -i cp images/N02105558__EVAL_10min_.mp4_1k/{} images/N02105558__EVAL_10min_.mp4_500
```