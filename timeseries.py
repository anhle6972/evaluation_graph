import pandas as pd
import re
import csv
import numpy as np
from collections import Counter

labels = ['pedestrian', 'motorbike', 'car', 'bus', 'truck']
df_detect = pd.DataFrame(columns=['vehicle', 'x', 'y', 't', 'b'])
df_groundtruth = pd.DataFrame(columns=['groundtruth_' + label for label in labels])

from collections import defaultdict

dict_detect = {}
with open('ntpn_fps_2/detect_result_ntpn_test_fps_2_roi_filtered.txt', 'r') as f:
    file = csv.reader(f)
    for row in file:
        frame_number = row[0]
        dict_detect[frame_number] = {label: [] for label in labels}
        boxes = np.array(row[2:]).reshape(-1, 5)  # each row in boxes is an box (type and 4 coordinates)
        for box in boxes:
            vehicle_type = box[0].split('(')[0]
            coordinates = [float(x) for x in box[1:]]
            dict_detect[frame_number][vehicle_type].append(coordinates)

import json

with open('detect.js', 'w') as f:
    f.write('var detect_json = ' + json.dumps(dict_detect))
print('Generated detect.js')

dict_groundtruth = {}
with open('ntpn_fps_2/ntpn_test_fps_2_groundtruth_180403_roi_filtered.txt', 'r') as f:
    file = csv.reader(f)
    for row in file:
        frame_number = row[0].split('/')[-1]
        dict_groundtruth[frame_number] = {label: [] for label in labels}
        boxes = np.array(row[2:]).reshape(-1, 5)  # each row in boxes is an box (type and 4 coordinates)
        for box in boxes:
            vehicle_type = box[0]
            coordinates = [float(x) for x in box[1:]]
            # print(frame_number, vehicle_type, coordinates)
            dict_groundtruth[frame_number][vehicle_type].append(coordinates)

with open('groundtruth.js', 'w') as f:
    f.write('var gd_json = ' + json.dumps(dict_groundtruth))
print('Generated groundtruth.js')
