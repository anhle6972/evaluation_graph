var gd_json = detect_json       // 180416: fake groundtruth var

// 180417
var global_cur_frame_idx = null;
var start_zoom_idx = 0;


const rawData = [];
const errorData = [];
const list_img_name = Object.keys(detect_json);
const number_img = list_img_name.length;

const ALL_LABEL = 'total'
const labels = ['pedestrian', 'motorbike', 'car', 'bus', 'truck', ALL_LABEL];
const DEFAULT_LABEL_DATA_COL = 1 + ALL_LABEL.indexOf(labels)
const MAIN_CHART_DATATABLE_ZOOM_COLOR_COL_IDX = 1 + 2*labels.length;         // 180517: calculate this index automatically

const max_dict = {};
const avg_rel_error = {};
for (const label of labels) {
    max_dict[label] = 0;
    avg_rel_error[label] = 0;
}

// add header: detect_pedestrian, detect_motorbike, ...
// groundtruth_pedestrian, groundtruth_motorbike, ...
// to rawData
const rawDataHeader = appendStr('detect_', labels).concat(appendStr('groundtruth_', labels));
rawDataHeader.unshift('frame_number');
rawData.push(rawDataHeader);

// add error header : error_pedestrian, error_motorbike, ...
const errorDataHeader = appendStr('error_', labels);
errorDataHeader.unshift('frame_number');
errorData.push(errorDataHeader);

// copy data from groundtruth.js and detect.js to rawData and errorData
var delta_ms = 1000/fps;
var current_time = first_frame_time;
for (const img_name of list_img_name) {
    const frame_number = img_name.match(/\d+/)[0];
    //console.log('current_time');
    //console.log(current_time);
    const dataRow = [current_time];
    // update current time
    current_time = current_time.slice();
    new_ms = current_time[3] + delta_ms;
    if (new_ms >= 1000) {
        current_time[3] = new_ms % 1000;
        var new_s = current_time[2] + new_ms/1000;
        if (new_s >= 60) {
            current_time[2] = new_s%60;
            var new_m = current_time[1] + new_s/60;
            if (new_m >= 60) {
                current_time[1] = new_m%60;
                var new_h = current_time[0] + new_m/60;
                if (new_h >= 24) {
                    current_time[0] = new_h%24;
                } else {
                    current_time[0] = new_h;
                }
            } else {
                current_time[1] = new_m;
            }
        } else {
            current_time[2] = new_s;
        }
    } else {
        current_time[3] = new_ms;
    }

    const errorRow = [parseInt(frame_number)];

    var n_all = 0;
    for (const label of labels) {
        const n_detect = label != ALL_LABEL ? detect_json[img_name][label].length : n_all;
        dataRow.push(n_detect);
        n_all += n_detect;
        if (max_dict[label] < n_detect) max_dict[label] = n_detect;
    }

    n_all = 0;  // reset n all to zero
    for (const label of labels) {
        const n_gdtruth = label != ALL_LABEL ? gd_json[img_name][label].length : n_all;
        dataRow.push(n_gdtruth);
        n_all += n_gdtruth;
        if (max_dict[label] < n_gdtruth) max_dict[label] = n_gdtruth;
    }
    for (const [idx, label] of labels.entries()) {
        let rel_error;
        const n_detect = dataRow[idx + 1];
        const n_gdtruth = dataRow[idx + 1 + labels.length];
        if (n_gdtruth == 0) {
            if (n_detect == 0) rel_error = 0;
            else rel_error = 1;
        }
        else rel_error = Math.abs(n_gdtruth - n_detect) / n_gdtruth;
        avg_rel_error[label] += rel_error;
        errorRow.push(rel_error);
    }
    rawData.push(dataRow);
    errorData.push(errorRow);
}
for (const label of labels) {
    avg_rel_error[label] /= number_img;
}

function capitalizeFirstLetter(string) {
    return string[0].toUpperCase() + string.slice(1);
}

// 180417
let cur_vehicle = 'motorbike';  // zoom chart also uses this variable

/*
var change_color_func = {
    calc: function(data, row) {
        var focusColor = 'color: green;';
        var defaultColor = 'color: #3366CC;';

        if (row >= start_zoom_idx && row <= start_zoom_idx + zoom_num_frames) {
          return focusColor;
        }
        return defaultColor;
    },
    type: 'string',
    role: 'style'
};
*/

function callback() {
    var data = google.visualization.arrayToDataTable(rawData, false);
    data.addColumn({type: 'string', role: 'style'})

    //console.log('aaa', data.getNumberOfColumns());
    
    const error_data = google.visualization.arrayToDataTable(errorData, false);

    //console.log('error_data', error_data.getNumberOfColumns());

    const view = new google.visualization.DataView(data);
    const gt_col_idx = DEFAULT_LABEL_DATA_COL+labels.length
    view.setColumns([0, DEFAULT_LABEL_DATA_COL, gt_col_idx, MAIN_CHART_DATATABLE_ZOOM_COLOR_COL_IDX]);
    view.hideColumns([gt_col_idx]);              // 180416
    const error_view = new google.visualization.DataView(error_data);
    error_view.setColumns([0, DEFAULT_LABEL_DATA_COL]);

    let cur_frame_idx = 0;
    let fps = 24;

    var options_mainplot = {
        title: capitalizeFirstLetter(cur_vehicle) + ' count detection over time',
        // focusTarget: 'category',
        animation: {
            duration: 0,            // update zoom region instantly
            easing: 'linear'        // update zoom region instantly
        },
        selectionMode: 'multiple',
        hAxis: {
            title: 'Time of day',
            gridlines: {
                count: 5,
                /*units: {
                    days: {format: ['MMM dd']},
                    hours: {format: ['HH:mm', 'ha']},
                }*/
            },
            /*minorGridlines: {
                units: {
                    hours: {format: ['hh:mm:ss a', 'ha']},
                    minutes: {format: ['HH:mm a Z', ':mm']}
                }
            }*/
        },
        vAxis: {
            viewWindow: {
                min: 0,
                max: max_dict[cur_vehicle]
            },
            title: 'Number of ' + cur_vehicle
        },
        crosshair: {
            trigger: 'selection',
            orientation: 'vertical',
            color: '#000000'
        },
        tooltip: {
            trigger: 'selection'
        },
        bars: 'vertical', // Required for Material Bar Charts.
        colors: [DETECTION_LINE_COLOR],
        curveType: 'function'
    };  

    const options_errorplot = {
        title: 'Relative error of ' + cur_vehicle + ' detection (Overall ' + avg_rel_error[cur_vehicle].toFixed(3) + '%)',
        animation: {
            duration: 1000,
            easing: 'out'
        },
        hAxis: {
            title: 'Frames'
        },
        vAxis: {
            viewWindow: {
                min: 0,
                max: 1
            },
            title: 'Percentage'
        },
        crosshair: {
            trigger: 'selection',
            orientation: 'vertical',
            color: '#000000'
        },
        tooltip: {
            trigger: 'selection'
        }
    };

    var main_chart = null;
    if (main_chart_type == 'line_chart') {
        main_chart = new google.visualization.LineChart(document.getElementById('chart_div'));
    } else if (main_chart_type == 'column_chart') {
        main_chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
    } else if (main_chart_type == 'area_chart') {
        main_chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
    } else if (main_chart_type == 'candle_stick_chart') {
        main_chart = new google.visualization.CandlestickChart(document.getElementById('chart_div'));
    } else if (main_chart_type == 'combo_chart') {
        main_chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
    } else if (main_chart_type == 'stepped_area_chart') {
        main_chart = new google.visualization.SteppedAreaChart(document.getElementById('chart_div'));
    }

    const error_chart = new google.visualization.LineChart(document.getElementById('error_div'));

    const all_btn = document.getElementById(ALL_LABEL);
    const pedestrian_btn = document.getElementById('pedestrian');
    const motorbike_btn = document.getElementById('motorbike');
    const car_btn = document.getElementById('car');
    const bus_btn = document.getElementById('bus');

    const truck_btn = document.getElementById('truck');
    const play_btn = document.getElementById('play');
    const pause_btn = document.getElementById('pause');
    const forward_btn = document.getElementById('forward');
    const backward_btn = document.getElementById('backward');
    const fpsControl = document.getElementById("myRange");
    const fpsBox = document.getElementById("demo");

    const detect_canvas = document.getElementById('detect_canvas');
    const gd_canvas = document.getElementById('gd_canvas');

    // 180517: draw red crowd
    if (crowd_threshold > 0) {
        vehicle_col_idx = labels.indexOf(ALL_LABEL) + 1;
        for(var k=0; k<data.getNumberOfRows(); k++) {
            vehicle_count = data.getValue(k, vehicle_col_idx);
            if (vehicle_count >= crowd_threshold) {
                data.setCell(k, MAIN_CHART_DATATABLE_ZOOM_COLOR_COL_IDX, 'color:' + crowd_region_color + '; fill-color:' + DETECTION_LINE_COLOR + '; fill-opacity:0.3;');
            }
        }
    }

    function drawChart() {
        /*pedestrian_btn.disable = motorbike_btn.disable = car_btn.disable = bus_btn.disable = truck_btn.disable = 1;
        google.visualization.events.addListener(main_chart, 'ready',
            function () {
                pedestrian_btn.disable = motorbike_btn.disable = car_btn.disable = bus_btn.disable = truck_btn.disable = truck_btn.disable - 1;
            });

        google.visualization.events.addListener(error_chart, 'ready',
            function () {
                pedestrian_btn.disable = motorbike_btn.disable = car_btn.disable = bus_btn.disable = truck_btn.disable = truck_btn.disable - 1;
            });*/

        //draw charts
        main_chart.draw(view, options_mainplot);
        error_chart.draw(error_view, options_errorplot);

        drawCanvas();

        // 180417
        idx = (global_cur_frame_idx!=null)?global_cur_frame_idx:0;
        updateSelection(idx);
        // end - 180417

        //this is for google chart's annoying bug
        google.visualization.events.addOneTimeListener(main_chart, 'onmouseover', function () {
            const selection = main_chart.getSelection();

            options_mainplot.focusTarget = 'category';
            options_mainplot.selectionMode = 'single';
            google.visualization.events.addOneTimeListener(main_chart, 'ready', function () {
                //console.log(selection);
                main_chart.setSelection(selection);
            });
            main_chart.draw(view, options_mainplot);
        });
    }

    function drawCanvas() {
        clearCanvas(detect_canvas);
        clearCanvas(gd_canvas);

        drawImage(gd_canvas, cur_frame_idx);
        drawImage(detect_canvas, cur_frame_idx);

        if (cur_vehicle == ALL_LABEL) {
            for(var i=0; i<labels.length-1; i++) {
                var tmp_vehicle = labels[i];
                drawRect(detect_canvas, detect_json[img_file(cur_frame_idx)][tmp_vehicle], vehicle_colors[tmp_vehicle]);
            }
        } else {
            const detect_coord = detect_json[img_file(cur_frame_idx)][cur_vehicle];
            drawRect(detect_canvas, detect_coord, vehicle_colors[cur_vehicle]);
        }
        
        
        if (cur_vehicle == ALL_LABEL) {
            for(var i=0; i<labels.length-1; i++) {
                var tmp_vehicle = labels[i];
                drawRect(gd_canvas, gd_json[img_file(cur_frame_idx)][tmp_vehicle], vehicle_colors[tmp_vehicle]);
            }
        } else {
            const gd_coord = gd_json[img_file(cur_frame_idx)][cur_vehicle];
            drawRect(gd_canvas, gd_coord, vehicle_colors[cur_vehicle]);
        }

    }

    function createSelectHandler(chart) {
        return function () {
            const selection = chart.getSelection();
            if (typeof selection !== 'undefined' && selection.length > 0) {
                // the array is defined and has at least one element
                //console.log(selection);
                cur_frame_idx = selection[0].row;
                updateSelection(cur_frame_idx);

                drawCanvas();
            }

        }
    }

    google.visualization.events.addListener(main_chart, 'select', createSelectHandler(main_chart));
    google.visualization.events.addListener(error_chart, 'select', createSelectHandler(error_chart));

    var last_zoom_region_row_idx = []
    var lass_zoom_region_row_value = []

    function updateSelection(row) {
        // 180417
        global_cur_frame_idx = row;
        // update zoom chart
        if (is_zoom == true) {
            start_zoom_idx = row - parseInt(zoom_num_frames/2);
            start_zoom_idx = (start_zoom_idx<0)?0:start_zoom_idx;
            start_zoom_idx = (start_zoom_idx + zoom_num_frames >= data.getNumberOfRows())?data.getNumberOfRows()-zoom_num_frames:start_zoom_idx;
            zoomDataArray = collectDataXFrames(start_zoom_idx, zoom_num_frames, data);
            row_idx_selection = row-start_zoom_idx;         // convert to idx range of zoom chart
            refreshZoomChart(zoomDataArray, 'line_chart', row_idx_selection, true) // refreshZoomChart(data, zoom_chart_type, is_one_line)
            
            // clear old zoom region
            for (var k=0; k<last_zoom_region_row_idx.length; k++) {
                row_idx = last_zoom_region_row_idx[k];
                data.setCell(row_idx, MAIN_CHART_DATATABLE_ZOOM_COLOR_COL_IDX, lass_zoom_region_row_value[k]);
            }
            // clear the list after retrieving
            last_zoom_region_row_idx = [];
            lass_zoom_region_row_value = [];           
            
            idx = labels.indexOf(cur_vehicle) + 1;
            for(var k=start_zoom_idx; k<start_zoom_idx+zoom_num_frames; k++) {
                lass_zoom_region_row_value.push(data.getValue(k, MAIN_CHART_DATATABLE_ZOOM_COLOR_COL_IDX));
                data.setCell(k, MAIN_CHART_DATATABLE_ZOOM_COLOR_COL_IDX, 'color:' + zoom_region_color + ';');
                last_zoom_region_row_idx.push(k);
            }

            if (is_draw_zoom_region == true) {
                main_chart.draw(view, options_mainplot);
            }            
        }
        
        //main_chart.setSelection([{row: row, column: 1}, {row: row, column: 2}]);  // 180416
        main_chart.setSelection([{row: row, column: 1}]);                           // 180416
        error_chart.setSelection([{row: row, column: 1}]);
    }

    function createOnClick(vehicle_type) {
        return function () {
            idx = labels.indexOf(vehicle_type) + 1;
            view.setColumns([0, idx, idx + labels.length, MAIN_CHART_DATATABLE_ZOOM_COLOR_COL_IDX]);
            view.hideColumns([idx + labels.length]);              // 180517: remove hard-code
            error_view.setColumns([0, idx]);

            options_mainplot.vAxis.viewWindow.max = max_dict[vehicle_type] + 1;

            options_mainplot.vAxis.title = 'Number of ' + vehicle_type;
            options_errorplot.title = 'Relative error of ' + vehicle_type + ' detection (Overall ' + avg_rel_error[vehicle_type].toFixed(3) + '%)'
            options_mainplot.title = capitalizeFirstLetter(vehicle_type) + ' count detection over time';
            cur_vehicle = vehicle_type;
            drawChart();

            // 180517: change focused button background color
            for (var i=0; i<labels.length; i++) {
                document.getElementById(labels[i]).style.backgroundColor = BUTTON_VEHICLE_DEFAULT_COLOR;
            }

            document.getElementById(cur_vehicle).style.backgroundColor = BUTTON_VEHICLE_FOCUS_COLOR;            
        }
    }

    
    all_btn.onclick = createOnClick(ALL_LABEL);     // 180517
    pedestrian_btn.onclick = createOnClick('pedestrian');
    motorbike_btn.onclick = createOnClick('motorbike');
    car_btn.onclick = createOnClick('car');
    bus_btn.onclick = createOnClick('bus');
    truck_btn.onclick = createOnClick('truck');
    
    function increaseSelection() {
        const selection = main_chart.getSelection();

        cur_frame_idx = (selection[0].row + 1) % number_img;
        updateSelection(cur_frame_idx);

        drawCanvas();
    }

    function decreaseSelection() {
        const selection = main_chart.getSelection();

        cur_frame_idx = selection[0].row - 1;
        if (cur_frame_idx < 0) cur_frame_idx += number_img;
        updateSelection(cur_frame_idx);

        drawCanvas();
    }

    let intervalID;
    let isPlaying = false;

    play_btn.onclick = function () {
        intervalID = setInterval(increaseSelection, 1000 / fps);
        play_btn.style.display = 'none';
        pause_btn.style.display = 'inline';
        pause_btn.style.width = '100%';
        //console.log('Start interval: ' + intervalID);
        isPlaying = true;
    };
    pause_btn.onclick = function () {
        clearInterval(intervalID);
        play_btn.style.display = 'inline';
        pause_btn.style.display = 'none';
        //console.log('Clear interval: ' + intervalID);
        isPlaying = false;

    };
    forward_btn.onclick = increaseSelection;
    backward_btn.onclick = decreaseSelection;

    fpsBox.innerHTML = fpsControl.value;
    fps = fpsControl.value;
    fpsControl.oninput = function () {
        fps = fpsControl.value;
        fpsBox.innerHTML = this.value;
        if (isPlaying) {
            clearInterval(intervalID);
            intervalID = setInterval(increaseSelection, 1000 / fps);
        }

    };
    drawChart();
    
    // 180517
    all_btn.click();    
}


google.charts.load('current', {'packages': ['corechart']});
google.charts.setOnLoadCallback(callback);

